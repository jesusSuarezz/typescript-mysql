"use strict";
//Utilizacion del patron de diseño 'sigleton'
//para prevenir multiples instancias de la clase
Object.defineProperty(exports, "__esModule", { value: true });
const mysql = require("mysql");
class Mysql {
    constructor() {
        this.conectado = false;
        console.log('Clase inicializada');
        this.conection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            port: 3306,
            database: 'node_db_dev'
        });
        this.conectarDB();
    }
    //Este es el patron singleton para que la instancia se inicialice solo una vez
    static get instance() {
        // Retorna la instancia que se inicializo o crea una nueva instancia de la clase
        return this._instance || (this._instance = new this());
    }
    static ejecutarQuery(query, callback) {
        //Llamamos a la instacia para que puedamos utilizar la conection ya que solo se puede usar si ya esta inicializado el contructor
        this.instance.conection.query(query, (err, results, fields) => {
            if (err) {
                console.log('Error en el query');
                console.log(err);
                return callback(err);
            }
            //Si se hizo con exito el query pero no encontro nada
            if (results.length === 0) {
                callback('El registro solicitado no existe');
            }
            else {
                callback(null, results);
            }
        });
    }
    //Es un metodo privado osea que solo se accede a el desde la misma clase
    conectarDB() {
        this.conection.connect((err) => {
            if (err) {
                console.log(err.message);
                return;
            }
            this.conectado = true;
            console.log('Base de datos online');
        });
    }
}
exports.default = Mysql;
