import { Router, Request, Response } from 'express'
import Mysql from '../mysql/mysql'

//Creamos una nueva instancia de Router
const router = Router()

router.get('/heroes', (req: Request, res: Response) => {
    
    const query = `SELECT * FROM heroes`
    
    Mysql.ejecutarQuery(query, (err:any, heroes:Object[]) => {
        if (err) {
            res.status(400).json({
                ok: false,
                error:err
            })
        } else {
            res.json({
                ok: true,
                heroes
            })
        }
    })
    
})

router.get('/heroes/:id', (req: Request, res: Response) => {

    //validamos el id que envia el cliente para evitar una inyeccion de datos
    const scapeId=Mysql.instance.conection.escape(req.params.id)
    const query = `SELECT * FROM heroes WHERE id=${scapeId}`
    
    Mysql.ejecutarQuery(query, (err:any, heroe:Object[]) => {
        if (err) {
            res.status(400).json({
                ok: false,
                error:err
            })
        } else {
            res.json({
                ok: true,
                heroe:heroe[0]
            })
        }
    })
    
})

export default router