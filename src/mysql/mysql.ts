//Utilizacion del patron de diseño 'sigleton'
//para prevenir multiples instancias de la clase

import mysql = require('mysql')

export default class Mysql{

    private static _instance: Mysql
    
    conection: mysql.Connection;
    
    conectado: boolean = false;

    constructor() {
        console.log('Clase inicializada');

        this.conection = mysql.createConnection(
            {
                host: 'localhost',
                user: 'root',
                password: '',
                port:3306,
                database: 'node_db_dev'
            }
        )

        this.conectarDB()
    }


    //Este es el patron singleton para que la instancia se inicialice solo una vez
    public static get instance() {
        // Retorna la instancia que se inicializo o crea una nueva instancia de la clase
        return this._instance || (this._instance = new this())
    }


    static ejecutarQuery(query: string, callback:Function ){
        //Llamamos a la instacia para que puedamos utilizar la conection ya que solo se puede usar si ya esta inicializado el contructor
        this.instance.conection.query(query, (err, results: Object[], fields) => {
            if (err) {
                console.log('Error en el query');
                console.log(err);

                return callback(err)
            }

            //Si se hizo con exito el query pero no encontro nada
            if (results.length === 0) {
                callback('El registro solicitado no existe')
            } else {
                callback(null, results)
            }
        })
    }


    //Es un metodo privado osea que solo se accede a el desde la misma clase
    private conectarDB() {
        this.conection.connect((err: mysql.MysqlError) => {
            if (err) {
                console.log(err.message);
                return
            }

            this.conectado = true
            console.log('Base de datos online');
        })
    }

}