import express = require('express')
import path = require('path')

export default class Server{
    public app: express.Application;
    public port: number;

    constructor(puerto: number) {
        this.port = puerto
        
        //Inicializamos la app
        this.app = express()
    }

    static init(puerto: number) {
        return new Server(puerto)
    }

    private publicFolder() {
        const publicPath = path.resolve(__dirname, '../public');
        this.app.use(express.static(publicPath))
    }

    //Funcion que inicia el servidor cuando se ejecuta
    start(callback: () => void) {
        
        this.app.listen(this.port, callback);
        this.publicFolder()
    }
}