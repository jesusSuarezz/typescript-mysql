import Server from './server/server';
import router from './router/router'
import Mysql from './mysql/mysql';

//Inicializamos nuestro servidor y le pasamos como parametro el numero de puerto
const server = Server.init(3000)

server.app.use(router)


//inicializamos la instacia que extraemos de la clase mysql
//Mysql.instance;


//Llamamos la funcion que inicia el server
server.start(() => {
    console.log('Servidor corriendo en el puerto 3000');
})